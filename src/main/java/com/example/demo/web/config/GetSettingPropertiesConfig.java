package com.example.demo.web.config;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by mjl on 2018/8/10.
 */
@AutoConfigureBefore(MultipartConfig.class)
@Component
@PropertySource(value = {"classpath:/properties/setting.properties"})
@ConfigurationProperties(prefix = "com.example")
public class GetSettingPropertiesConfig {
    private static String localRootPath;
    private static String localTempPath;
    private static String gzhAppid;
    private static String gzhAppsecret;
    private static String gzhSqUrl;

    public static String getLocalRootPath() {
        return localRootPath;
    }

    public void setLocalRootPath(String localRootPath) {
        this.localRootPath = localRootPath;
    }

    public static String getLocalTempPath() {
        return localTempPath;
    }

    public void setLocalTempPath(String localTempPath) {
        this.localTempPath = localTempPath;
    }

    public static String getGzhAppid() {
        return gzhAppid;
    }

    public void setGzhAppid(String gzhAppid) {
        this.gzhAppid = gzhAppid;
    }

    public static String getGzhAppsecret() {
        return gzhAppsecret;
    }

    public void setGzhAppsecret(String gzhAppsecret) {
        this.gzhAppsecret = gzhAppsecret;
    }

    public static String getGzhSqUrl() {
        return gzhSqUrl;
    }

    public void setGzhSqUrl(String gzhSqUrl) {
        this.gzhSqUrl = gzhSqUrl;
    }
}
