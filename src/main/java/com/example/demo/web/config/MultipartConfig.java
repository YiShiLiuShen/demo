package com.example.demo.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;
import java.io.File;

/**
 * Created by mjl on 2018/8/9.
 */
@Configuration
@EnableConfigurationProperties(GetSettingPropertiesConfig.class)
public class MultipartConfig {

    @Autowired
    private GetSettingPropertiesConfig getSettingPropertiesConfig;
    /**
     * 文件上传临时路径
     */
    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        String location = getSettingPropertiesConfig.getLocalTempPath();
        File tmpFile = new File(location);
        if (!tmpFile.exists()) {
            tmpFile.mkdirs();
        }
        System.out.println(tmpFile.getAbsolutePath());
        factory.setLocation(location);
        return factory.createMultipartConfig();
    }
}
