package com.example.demo.web.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.web.wxUtil.WeiXinUtil;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;

/**
 * 转换微信url,转传成可直接在微信端直接看的url
 * Created by mjl on 2018/8/4.
 */
public class TransformationURL {

    public static final String API_STR1 = "http://vv.video.qq.com/getinfo?vids=";

    public static final String API_STR2 = "&platform=101001&charge=0&otype=json";

    public static String getTXVideosUrl(String oldUrl) {
        // 1.通过oldUrl获取vid
        // 截取url把 .html 后面的内容都截掉
        if(StringUtils.isNotBlank(oldUrl) && oldUrl.substring(0,oldUrl.indexOf(".html")+5).endsWith(".html")){
            String vid = oldUrl.substring(oldUrl.lastIndexOf("/")+1,oldUrl.lastIndexOf(".html"));
            if(StringUtils.isNotBlank(vid)){
                try {
                    // 2. 组合api和参数
                    StringBuffer sbf_reqeust_url = new StringBuffer(API_STR1);
                    sbf_reqeust_url.append(vid);
                    sbf_reqeust_url.append(API_STR2);
                    String result = HttpConnectionTool.getInstance().sendGet(sbf_reqeust_url.toString());
                    // 3.处理返回结果
                    String handler_result1 = result.replace("QZOutputJson=","");
                    String handler_result2 = handler_result1.substring(0,handler_result1.lastIndexOf(";"));
                    JSONObject json = JSONObject.parseObject(handler_result2);
                    JSONObject vl_json = JSONObject.parseObject(json.get("vl").toString());
                    JSONArray vl_vi_json = JSONObject.parseArray(vl_json.get("vi").toString());
                    JSONObject vl_vi0_json = JSONObject.parseObject(vl_vi_json.get(0).toString());
                    String fvkey = vl_vi0_json.get("fvkey").toString();
                    String fn = vl_vi0_json.get("fn").toString();
                    JSONObject vl_vi0_ul_json = JSONObject.parseObject(vl_vi0_json.get("ul").toString());
                    JSONArray vl_vi0_ul_ui_json = JSONObject.parseArray(vl_vi0_ul_json.get("ui").toString());
                    JSONObject vl_vi0_ul_ui_url0 = JSONObject.parseObject(vl_vi0_ul_ui_json.get(0).toString());
                    String url0 = vl_vi0_ul_ui_url0.get("url").toString();

                    return url0 + fn + "?vkey=" + fvkey ;
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        String oldUrl = "https://v.qq.com/x/cover/yb5x7kfdqhypo10/y0027zsdxhb.html?skdfajksdjlfjasld=sdfsdlk;s?html";
//        System.out.println(getTXVideosUrl(oldUrl));

        System.out.println(oldUrl.substring(0,oldUrl.indexOf(".html")+5));
    }
}

