package com.example.demo.web.util;

import com.example.demo.web.config.GetSettingPropertiesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import sun.misc.BASE64Decoder;

import javax.xml.bind.DatatypeConverter;
import java.io.*;

/**
 * Created by mjl on 2018/8/8.
 */
//@EnableConfigurationProperties(GetSettingPropertiesConfig.class)
public class Base64ToImageUtils {
    private static final String IMG_PER_CUS = "IMG_PER_CUS";    // 个性化定制对应枚举类型 PathEnum

//    @Autowired
//    private GetSettingPropertiesConfig getSettingPropertiesConfig;

    /**
     * base64字符串转换成图片
     * @param base64Str
     * @return
     * @throws IOException
     */
    public static String  base64ToImage(String base64Str) throws IOException {
        String path = FileTools.getInstance().getPaht(IMG_PER_CUS);
        String destPaht = GetSettingPropertiesConfig.getLocalRootPath() + path;

        File destFile = new File(destPaht);
        if(!destFile.exists()){
            destFile.mkdirs();
        }
        String[] strings = base64Str.split(",");
        String extension;
        switch (strings[0]) {//check image's extension
            case "data:image/jpeg;base64":
                extension = "jpeg";
                break;
            case "data:image/png;base64":
                extension = "png";
                break;
            default://should write cases for more images types
                extension = "jpg";
                break;
        }

        String newFileName = System.currentTimeMillis() + "." +extension;
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] data = decoder.decodeBuffer(strings[1]);
//        byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
        File file = new File(destFile.getAbsolutePath(),newFileName);
        try(FileOutputStream fos = new FileOutputStream(file)){
            fos.write(data);
        }
        return path + newFileName;
    }

    public static void main(String[] args){
        String test = "";
        try {
            base64ToImage(test);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
