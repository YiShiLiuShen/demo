package com.example.demo.web.util;

import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Johcha on 2017/1/2 0002.
 */
public class SystemTool {

    private volatile static SystemTool instance;

    private static final String DefaultDate  = "yyyy-MM-dd HH:mm:ss";

    public static SystemTool getInstance() {
        if (instance == null) {
            synchronized (SystemTool.class){
                instance = new SystemTool();
            }
        }
        return instance;
    }

    /***
     * 根据默认日期格式获取当前日期
     * @return  默认格式的当前日期
     */
    public Date getDefaultCurrentDate(){
        return getDate(DefaultDate, new Date());
    }

    /***
     * 根据默认日期格式获取当前日期之前的日期
     * @param d 当前日期
     * @param hour 日期差(按小时)
     * @return  默认格式的当前日期
     */
    public Date getDateBefore(Date d, int hour) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.HOUR, now.get(Calendar.HOUR) - hour);
        return now.getTime();
    }

    /***
     * 根据默认日期格式获取当前日期之后的日期
     * @param d 当前日期
     * @param hour 日期差(按小时)
     * @return  默认格式的当前日期
     */
    public Date getDateAfter(Date d, int hour) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.HOUR, now.get(Calendar.HOUR) + hour);
        return now.getTime();
    }

    /***
     * 根据日期格式获取日期
     * @param format  日期格式字符串
     * @param date  日期
     * @return  转换后的日期
     */
    public Date getDate(String format, Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        String dateString = formatter.format(date);
        Date d = null;
        try {
            d = formatter.parse(dateString);
        } catch (ParseException e) {
            System.out.println(e.toString());
            return null;
        }
        return d;
    }

    /***
     * 利用MD5进行加密
     * @param str  待加密的字符串
     * @return  加密后的字符串
     * @throws NoSuchAlgorithmException  没有这种产生消息摘要的算法
     * @throws UnsupportedEncodingException
     */
    public String EncoderByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //确定计算方法
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        BASE64Encoder base64en = new BASE64Encoder();
        //加密后的字符串
        String newStr = base64en.encode(md5.digest(str.getBytes("utf-8")));
        return newStr;
    }

    /***
     * GBK TO UTF8
     * @param s
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getUtf8String(String s) throws UnsupportedEncodingException
    {
        StringBuffer sb = new StringBuffer();
        sb.append(s);
        String xmlString = "";
        String xmlUtf8 = "";
        xmlString = new String(sb.toString().getBytes("GBK"));
        xmlUtf8 = URLEncoder.encode(xmlString , "GBK");

        return URLDecoder.decode(xmlUtf8, "UTF-8");
    }

    /***
     * 根据Unicode编码完美的判断中文汉字和符号
     * @param c
     * @return
     */
    public boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }

    /***
     * 完整的判断中文汉字和符号
     * @param strName
     * @return
     */
    public boolean isChinese(String strName) {
        char[] ch = strName.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (isChinese(c)) {
                return true;
            }
        }
        return false;
    }

    /***
     * 汉字转阿拉伯数字
     * @param c
     * @return
     */
    public char chinese2number(char c) {
        char result = c;
        switch (c) {
            case '一':
                result = '1';
                break;
            case '二':
                result = '2';
                break;
            case '三':
                result = '3';
                break;
            case '四':
                result = '4';
                break;
            case '五':
                result = '5';
                break;
            case '六':
                result = '6';
                break;
            case '七':
                result = '7';
                break;
            case '八':
                result = '8';
                break;
            case '九':
                result = '9';
                break;
        }
        return result;
    }


    /**
     * InputStream ---> String
     * @param is
     * @return
     */
    public String convertInputStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "/n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    /*
     * 将时间戳转换为时间
     */
    public Date stampToDate(String s) throws ParseException {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return simpleDateFormat.parse(res);
    }

}
