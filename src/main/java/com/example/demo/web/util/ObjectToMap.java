package com.example.demo.web.util;


import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Administrator on 2017/3/9.
 */
public class ObjectToMap {
    private volatile static ObjectToMap instance;

    public static ObjectToMap getInstance(){
        if(instance==null){
            synchronized (ObjectToMap.class){
                instance = new ObjectToMap();
            }
        }
        return instance;
    }

    /**
     * 获取本类属性
     * @param o
     * @return
     */
    public Map<String,Object> objToMap(Object o){
        Field[] fields=o.getClass().getDeclaredFields();
        Map<String,Object> infoMap=new HashMap();
        for(int i=0;i<fields.length;i++){
            infoMap.put(fields[i].getName(),getFieldValueByName(fields[i].getName(), o));
        }
        return infoMap;
    }

    private Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取包含父类的属性
     * @param o
     * @return
     */
    public Map<String,Object> objToMapContainParent(Object o){
        Map<String,Object> infoMap=new HashMap();
        List<Field> fieldList = new ArrayList<>() ;
        Class tempClass = o.getClass();
        while (tempClass != null) {//当父类为null的时候说明到达了最上层的父类(Object类).
            fieldList.addAll(Arrays.asList(tempClass .getDeclaredFields()));
            tempClass = tempClass.getSuperclass(); //得到父类,然后赋给自己
        }
        for (Field f : fieldList) {
            infoMap.put(f.getName(),getFieldValueByName(f.getName(), o));
        }
        return infoMap;
    }

    public static void main(String[] args ){


    }
}
