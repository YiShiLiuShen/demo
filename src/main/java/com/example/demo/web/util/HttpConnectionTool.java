package com.example.demo.web.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Johcha on 2017/3/10 0010.
 */
public class HttpConnectionTool {

    private volatile static HttpConnectionTool instance;

    public static HttpConnectionTool getInstance() {
        if (instance == null) {
            synchronized (HttpConnectionTool.class){
                instance = new HttpConnectionTool();
            }
        }
        return instance;
    }

    public String getResponseString(InputStream inputStream) throws IOException {
        // 当正确响应时处理数据
        StringBuffer sb = new StringBuffer();
        String readLine;
        BufferedReader responseReader;
        // 处理响应流，必须与服务器响应流输出的编码一致
        responseReader = new BufferedReader(new InputStreamReader(inputStream));
        while ((readLine = responseReader.readLine()) != null) {
            sb.append(readLine).append("\n");
        }
        responseReader.close();
        return sb.toString();
    }

    /***
     * 后台进行http get请求
     * @param path 请求地址
     */
    public String sendGet(String path) throws IOException {
        //String path = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx0fd2c6c6afe66bcc&secret=88b2bf0d786da5a0a0acd94d472d13db";
        StringBuilder url = new StringBuilder(path);
        HttpURLConnection conn = (HttpURLConnection)new URL(url.toString()).openConnection();
        conn.setConnectTimeout(5000);
        conn.setRequestMethod("GET");
        int responseCode = conn.getResponseCode();
        if(responseCode == 200){
            // 当正确响应时处理数据
            return getResponseString(conn.getInputStream());
        }
        return null;
    }

    /***
     * 后台进行http post请求
     * @param path 请求地址
     */
    public String sendPost(String path, String params) throws IOException {
        //String path = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx0fd2c6c6afe66bcc&secret=88b2bf0d786da5a0a0acd94d472d13db";
//        StringBuilder data = new StringBuilder();
//        if(params!=null && !params.isEmpty()){
//            for(Map.Entry<String, Object> entry : params.entrySet()){
//                data.append(entry.getKey()).append("=");
//                data.append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
//                data.append("&");
//            }
//            data.deleteCharAt(data.length() - 1);
//        }
        byte[] entity = params.toString().getBytes();//生成实体数据
        HttpURLConnection conn = (HttpURLConnection) new URL(path).openConnection();
        conn.setConnectTimeout(5000);
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);//允许对外输出数据
        conn.setRequestProperty("Content-Type", "application/json");
//        conn.setRequestProperty("Content-Length", String.valueOf(entity.length));
        OutputStream outStream = conn.getOutputStream();
        outStream.write(entity);
        int responseCode = conn.getResponseCode();
        if(responseCode == 200){
            // 当正确响应时处理数据
            return getResponseString(conn.getInputStream());
        }
        return null;
    }

}
