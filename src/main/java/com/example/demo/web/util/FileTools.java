package com.example.demo.web.util;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;


/**
 * Created by mjl on 2018/5/5.
 */
public class FileTools {
    private volatile static FileTools instance;

    public static FileTools getInstance(){
        if(instance==null){
            synchronized (FileTools.class){
                instance = new FileTools();
            }
        }
        return instance;
    }

    public String getName(String path){
        if(path!=null){
            return path.substring(path.lastIndexOf("/")+1,path.length());
        }
        return null;
    }

    public byte[] getByteByPath(String path){
        byte[] bytesArray = null;
        try {
            File pathFile = new File(ResourceUtils.getURL("classpath:").getPath());
            File file = new File(pathFile.getAbsolutePath(),path);
            bytesArray = new byte[(int) file.length()];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bytesArray;
    }

    // 通过type 获取 path
    public String getPaht(String type){
        String path = null;
        if(type.equals(PathEnum.PATH_UPLOAD_FLOW_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_FLOW_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_BUILDS_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_BUILDS_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_QR_CODE_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_QR_CODE_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_HOUSE_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_HOUSE_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_HOUSE_PLAN_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_HOUSE_PLAN_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_SPACE_PLAN_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_SPACE_PLAN_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_PRODUCTE_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_PRODUCTE_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_PER_CUS_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_PER_CUS_IMAGES.getPath();
        }
        else if(type.equals(PathEnum.PATH_UPLOAD_XDYL_QR_CODE_IMAGES.getType())){
            path = PathEnum.PATH_UPLOAD_XDYL_QR_CODE_IMAGES.getPath();
        }

        return path;
    }
}
