package com.example.demo.web.util;

/**
 * Created by mjl on 2018/5/25.
 */
public  enum PathEnum {
    // type 明明给则 图片以"IMG_"开头；音频以"AUDIO_"开头；
    PATH_UPLOAD_NONE("NONE",""),
    PATH_UPLOAD_ASSETS("ASSETS","upload/"),
    PATH_UPLOAD_FLOW_IMAGES("IMG_FLOW",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/flow_images/"),
    PATH_UPLOAD_BUILDS_IMAGES("IMG_BUILDS",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/builds_images/"),
    PATH_UPLOAD_QR_CODE_IMAGES("IMG_QR_CODE",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/qr_code_images/"),
    PATH_UPLOAD_HOUSE_IMAGES("IMG_HOUSE",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/house_images/"),
    PATH_UPLOAD_HOUSE_PLAN_IMAGES("IMG_HOUSE_PLAN",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/house_plan_images/"),
    PATH_UPLOAD_SPACE_PLAN_IMAGES("IMG_SPACE_PLAN",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/space_plan_images/"),
    PATH_UPLOAD_PRODUCTE_IMAGES("IMG_PRODUCT",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/house_product_images/"),
    PATH_UPLOAD_PER_CUS_IMAGES("IMG_PER_CUS",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/personal_customiztion_images/"),
    PATH_UPLOAD_XDYL_QR_CODE_IMAGES("IMG_XDYL_QR_CODE",PathEnum.PATH_UPLOAD_ASSETS.getPath() + "images/xdyl_qr_code_images/");

    private String type;
    private String path;

    private PathEnum(String type, String path) {
        this.type = type;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
