package com.example.demo.web.wxUtil;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.web.config.GetSettingPropertiesConfig;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mjl on 2018/7/28.
 */
public class WeiXinUtil {
    /**
     * 通过code换取网页授权access_token
     * @param code
     * @return
     */
    public static JSONObject getToken(String code) throws IOException {
        StringBuffer sbf = new StringBuffer("https://api.weixin.qq.com/sns/oauth2/access_token?appid=");
        sbf.append(GetSettingPropertiesConfig.getGzhAppid());
        sbf.append("&secret=");
        sbf.append(GetSettingPropertiesConfig.getGzhAppsecret());
        sbf.append("&code=");
        sbf.append(code);
        sbf.append("&grant_type=authorization_code");
        JSONObject json = httpRequest(sbf.toString(), "GET", null);
        // 访问微信接口
        return json;
    }

    /**
     * 刷新token
     * @return
     * @throws IOException
     */
    public static JSONObject refreshToken(String refresh_token) throws IOException {
        StringBuffer sbf = new StringBuffer("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=");
        sbf.append(GetSettingPropertiesConfig.getGzhAppid());
        sbf.append("&grant_type=refresh_token");
        sbf.append("&refresh_token=");
        sbf.append(refresh_token);
        JSONObject json = httpRequest(sbf.toString(), "GET", null);
        return json;
    }

    /**
     * 拉取用户信息(需scope为 snsapi_userinfo)
     * @param access_token
     * @param openId
     * @return
     * @throws IOException
     */
    public static JSONObject getWxUserInfo(String access_token,String openId) throws IOException {
        StringBuffer sbf = new StringBuffer("https://api.weixin.qq.com/sns/userinfo?access_token=");
        sbf.append(access_token);
        sbf.append("&openid=");
        sbf.append(openId);
        sbf.append("&lang=zh_CN");
        JSONObject json = httpRequest(sbf.toString(), "GET", null);
        return json;
    }

    public static JSONObject getGZHWxUserInfoByCode(String code) throws IOException {
        JSONObject jsonObject = getToken(code);
        Object o = jsonObject.get("access_token");
        if(o!=null && !"".equals(o.toString())){
            String access_token = o.toString();
            String openid = jsonObject.get("openid").toString();
            jsonObject = getWxUserInfo(access_token,openid);
        }
        return jsonObject;
    }

    /**
     * 访问微信接口
     * @param requestUrl
     * @param requestMethod
     * @param outputStr
     * @return
     * @throws IOException
     */
    public static JSONObject httpRequest(String requestUrl, String requestMethod, String outputStr) throws IOException {
        JSONObject jsonObject = null;
        InputStream inputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        StringBuffer buffer = new StringBuffer();
        try {
            // 创建SSLContext对象，并使用我们指定的信任管理器初始化
            TrustManager[] tm = { new MyX509TrustManager() };
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new java.security.SecureRandom());
            // 从上述SSLContext对象中得到SSLSocketFactory对象
            SSLSocketFactory ssf = sslContext.getSocketFactory();

            URL url = new URL(requestUrl);
            HttpsURLConnection httpUrlConn = (HttpsURLConnection) url.openConnection();
            httpUrlConn.setSSLSocketFactory(ssf);

            httpUrlConn.setDoOutput(true);
            httpUrlConn.setDoInput(true);
            httpUrlConn.setUseCaches(false);
            // 设置请求方式（GET/POST）
            httpUrlConn.setRequestMethod(requestMethod);

            if ("GET".equalsIgnoreCase(requestMethod))
                httpUrlConn.connect();

            // 当有数据需要提交时
            if (null != outputStr) {
                OutputStream outputStream = httpUrlConn.getOutputStream();
                // 注意编码格式，防止中文乱码
                outputStream.write(outputStr.getBytes("UTF-8"));
                outputStream.close();
            }

            // 将返回的输入流转换成字符串
            inputStream = httpUrlConn.getInputStream();
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            bufferedReader = new BufferedReader(inputStreamReader);

            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            // 释放资源
            inputStream.close();
            inputStream = null;
            httpUrlConn.disconnect();
            jsonObject = JSONObject.parseObject(buffer.toString());
        } catch (ConnectException ce) {
            ce.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(bufferedReader!=null)
                bufferedReader.close();
            if(inputStreamReader!=null)
                inputStreamReader.close();
            if(inputStream!=null)
                inputStream.close();
        }
        return jsonObject;
    }

    /**
     * 过滤emoji符号字符
     * @param source
     * @return
     */
    public static String filterEmoji(String source) {
        if(source != null)
        {
            Pattern emoji = Pattern.compile ("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",Pattern.UNICODE_CASE | Pattern . CASE_INSENSITIVE ) ;
            Matcher emojiMatcher = emoji.matcher(source);
            if ( emojiMatcher.find())
            {
                source = emojiMatcher.replaceAll("");
                return source ;
            }
            return source;
        }
        return source;
    }

}
