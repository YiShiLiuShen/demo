package com.example.demo.web.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 地址县/区表(tab_county)
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@TableName("tab_county")
public class County implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer regionId;
    /**
     * 地区名称
     */
    private String regionName;
    /**
     * 父地区ID
     */
    private Integer cityId;


    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "County{" +
        "regionId=" + regionId +
        ", regionName=" + regionName +
        ", cityId=" + cityId +
        "}";
    }
}
