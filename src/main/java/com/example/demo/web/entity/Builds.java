package com.example.demo.web.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@TableName("tab_builds")
public class Builds implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 楼盘名称
     */
    private String buildsName;
    /**
     * 楼盘url
     */
    private String buildsUrl;
    /**
     * 创建时间
     */
    private Date buildsCreateTime;
    /**
     * 创建人
     */
    private Integer buildsCreateUser;
    /**
     * 排序
     */
    private Integer buildsOrder;
    /**
     * 省
     */
    private String buildsProvince;
    /**
     * 市
     */
    private Integer buildsCity;
    /**
     * 县
     */
    private Integer buildsCounty;
    /**
     * 热门楼盘：{1-是；0-否}
     */
    private Integer buildsHot;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBuildsName() {
        return buildsName;
    }

    public void setBuildsName(String buildsName) {
        this.buildsName = buildsName;
    }

    public String getBuildsUrl() {
        return buildsUrl;
    }

    public void setBuildsUrl(String buildsUrl) {
        this.buildsUrl = buildsUrl;
    }

    public Date getBuildsCreateTime() {
        return buildsCreateTime;
    }

    public void setBuildsCreateTime(Date buildsCreateTime) {
        this.buildsCreateTime = buildsCreateTime;
    }

    public Integer getBuildsCreateUser() {
        return buildsCreateUser;
    }

    public void setBuildsCreateUser(Integer buildsCreateUser) {
        this.buildsCreateUser = buildsCreateUser;
    }

    public Integer getBuildsOrder() {
        return buildsOrder;
    }

    public void setBuildsOrder(Integer buildsOrder) {
        this.buildsOrder = buildsOrder;
    }

    public String getBuildsProvince() {
        return buildsProvince;
    }

    public void setBuildsProvince(String buildsProvince) {
        this.buildsProvince = buildsProvince;
    }

    public Integer getBuildsCity() {
        return buildsCity;
    }

    public void setBuildsCity(Integer buildsCity) {
        this.buildsCity = buildsCity;
    }

    public Integer getBuildsCounty() {
        return buildsCounty;
    }

    public void setBuildsCounty(Integer buildsCounty) {
        this.buildsCounty = buildsCounty;
    }

    public Integer getBuildsHot() {
        return buildsHot;
    }

    public void setBuildsHot(Integer buildsHot) {
        this.buildsHot = buildsHot;
    }

    @Override
    public String toString() {
        return "Builds{" +
        "id=" + id +
        ", buildsName=" + buildsName +
        ", buildsUrl=" + buildsUrl +
        ", buildsCreateTime=" + buildsCreateTime +
        ", buildsCreateUser=" + buildsCreateUser +
        ", buildsOrder=" + buildsOrder +
        ", buildsProvince=" + buildsProvince +
        ", buildsCity=" + buildsCity +
        ", buildsCounty=" + buildsCounty +
        ", buildsHot=" + buildsHot +
        "}";
    }
}
