package com.example.demo.web.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 地址城市表(tab_city)
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@TableName("tab_city")
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer cityId;
    private String cityName;
    private String provinceId;
    private String firstLetter;
    private Integer isHot;
    private Integer state;


    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "City{" +
        "cityId=" + cityId +
        ", cityName=" + cityName +
        ", provinceId=" + provinceId +
        ", firstLetter=" + firstLetter +
        ", isHot=" + isHot +
        ", state=" + state +
        "}";
    }
}
