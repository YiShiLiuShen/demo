package com.example.demo.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@TableName("tab_advertisement")
public class Advertisement implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 标题
     */
    private String adTitle;
    /**
     * 图片地址
     */
    private String adPicUrl;
    /**
     * 视频地址
     */
    private String adVideoUrl;
    /**
     * 排序
     */
    private Integer adOrder;
    /**
     * 创建时间
     */
    private Date adCreateTime;
    /**
     * 创建人
     */
    private Integer adCreateUser;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle;
    }

    public String getAdPicUrl() {
        return adPicUrl;
    }

    public void setAdPicUrl(String adPicUrl) {
        this.adPicUrl = adPicUrl;
    }

    public String getAdVideoUrl() {
        return adVideoUrl;
    }

    public void setAdVideoUrl(String adVideoUrl) {
        this.adVideoUrl = adVideoUrl;
    }

    public Integer getAdOrder() {
        return adOrder;
    }

    public void setAdOrder(Integer adOrder) {
        this.adOrder = adOrder;
    }

    public Date getAdCreateTime() {
        return adCreateTime;
    }

    public void setAdCreateTime(Date adCreateTime) {
        this.adCreateTime = adCreateTime;
    }

    public Integer getAdCreateUser() {
        return adCreateUser;
    }

    public void setAdCreateUser(Integer adCreateUser) {
        this.adCreateUser = adCreateUser;
    }

    @Override
    public String toString() {
        return "Advertisement{" +
        "id=" + id +
        ", adTitle=" + adTitle +
        ", adPicUrl=" + adPicUrl +
        ", adVideoUrl=" + adVideoUrl +
        ", adOrder=" + adOrder +
        ", adCreateTime=" + adCreateTime +
        ", adCreateUser=" + adCreateUser +
        "}";
    }
}
