package com.example.demo.web.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 地址省表(tab_province)
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@TableName("tab_province")
public class Province implements Serializable {

    private static final long serialVersionUID = 1L;

    private String provinceId;
    private String provinceName;


    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    @Override
    public String toString() {
        return "Province{" +
        "provinceId=" + provinceId +
        ", provinceName=" + provinceName +
        "}";
    }
}
