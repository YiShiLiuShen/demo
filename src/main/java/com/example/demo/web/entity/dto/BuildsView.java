package com.example.demo.web.entity.dto;

import com.example.demo.web.entity.Builds;

/**
 * Created by mjl on 2018/8/15.
 */
public class BuildsView extends Builds{
    private String provinceName;
    private String cityName;
    private String regionName;

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
