package com.example.demo.web.controller;

import com.example.demo.web.config.GetSettingPropertiesConfig;
import com.example.demo.web.util.FileTools;
import com.example.demo.web.util.R;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;


/**
 * Created by mjl on 2018/3/31.
 */
@Controller
@EnableConfigurationProperties(GetSettingPropertiesConfig.class)
public class UploadController extends AbstractController {

    @Autowired
    private GetSettingPropertiesConfig getSettingPropertiesConfig;

    //  文件上传
    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    @ResponseBody
    public R upload(
        @RequestParam("file") MultipartFile file,
        @RequestParam("type") String type
    ) throws IOException {
        InputStream is = null;
        OutputStream out = null;
        try {
            if(StringUtils.isBlank(type)){
                return R.error("路径类型错误，type:"+type);
            }
            if(file.isEmpty()){
                return R.error("文件不能为空！");
            }
            String path = FileTools.getInstance().getPaht(type);
            if(path==null){
                return R.error("未在指定类型内，请在枚举中添加此类型==>type:"+type);
            }

            //  获取文件名
            String fileName = file.getOriginalFilename();
            String fileType = fileName.substring(fileName.lastIndexOf("."),fileName.length());  // 文件类型
            String destPaht = getSettingPropertiesConfig.getLocalRootPath() + path;
            // 文件路径
            File destFile = new File(destPaht);
            if(!destFile.exists()){
                destFile.mkdirs();
            }
            String newFileName = System.currentTimeMillis() + fileType;

            is = file.getInputStream();
            out = new FileOutputStream(new File(destFile.getAbsolutePath(),newFileName));  // 文件输出
            int length = 0;
            byte [] buffer = new byte[128];
            while((length = is.read(buffer))!=-1)
            {
                out.write(buffer,0, length);
            }
            this.logger.info("上传文件路径：{}",new File(destFile.getAbsolutePath(),newFileName).getAbsolutePath());
            return R.ok().put("newFilePath",path + newFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if (is != null) {
                    is.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return R.error("文件上传异常！");
    }

    /**
     * springboot 获取图片接口
     * @param path
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/getImage", method = RequestMethod.GET,
            produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage(
            @RequestParam(value = "path",required = false) String path
    ) throws IOException {
        if(path==null)
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(null);
        InputStream is = null;
        ByteArrayOutputStream outp = null;
        try{
            File pathFile = new File(getSettingPropertiesConfig.getLocalRootPath());
            File file = new File(pathFile.getAbsolutePath(),path);

            is = new FileInputStream(file);
            outp = new ByteArrayOutputStream();

            byte[] b = new byte[1024];
            int ch ;
            while((ch=is.read(b))!=-1){
                outp.write(b,0,ch);
            }
            outp.close();
            is.close();
            byte[] bytes = outp.toByteArray();

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(bytes);
        }catch (Exception e){
            logger.error("获取图片；{}，异常{}",getSettingPropertiesConfig.getLocalRootPath() + path,e.getMessage());
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(null);
        }finally {
            outp.close();
            is.close();
        }

    }


}
