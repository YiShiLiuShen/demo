package com.example.demo.web.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 地址县/区表(tab_county) 前端控制器
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Controller
@RequestMapping("/county")
public class CountyController {

}

