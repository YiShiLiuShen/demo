package com.example.demo.web.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.example.demo.web.controller.AbstractController;
import com.example.demo.web.entity.User;
import com.example.demo.web.service.UserService;
import com.example.demo.web.util.SystemTool;
import com.google.code.kaptcha.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mjl on 2018/7/17.
 */
@Controller
public class LoginController extends AbstractController {

    @Autowired
    private UserService userService;

    /**
     * 登入
     * @param userName      用户名
     * @param passWord      密码
     * @param verifyCode    验证码
     * @param model         返回html的绑定数据
     * @param request
     * @return
     */
    @RequestMapping(value = "login", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public String login(
            String userName,
            String passWord,
            String verifyCode,
            String rememberUserName,
            Model model,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        HttpSession session = request.getSession();
        String captchaTxt = (String) session.getAttribute(Constants.KAPTCHA_SESSION_KEY);
        if(!captchaTxt.equals(verifyCode)){
            model.addAttribute("msg","验证码错误");
            return "admin";
        }
        User user = userService.selectOne(new EntityWrapper<User>().eq("user_name",userName));
        String inputPassWord = SystemTool.getInstance().EncoderByMd5(passWord);
        if(user==null || !inputPassWord.equals(user.getPassWord())){
            model.addAttribute("msg","账号或密码错误");
            return "admin";
        }
        user.setPassWord("");
        session.setAttribute("isLogin",true);
        session.setAttribute("user",user);
        Cookie nameCookie = new Cookie("username",user.getUserName());
        nameCookie.setMaxAge(3600*24);
        // 记住我的账号
        if("on".equals(rememberUserName)){
            Cookie rememberMeCookie = new Cookie("rememberme","true");
            rememberMeCookie.setMaxAge(3600*24);
            response.addCookie(rememberMeCookie);
        }else{
            Cookie rememberMeCookie = new Cookie("rememberme","false");
            rememberMeCookie.setMaxAge(3600*24);
            response.addCookie(rememberMeCookie);
        }
        nameCookie.setPath("/");
        response.addCookie(nameCookie);
        return "admin/adminMain";
    }
}
