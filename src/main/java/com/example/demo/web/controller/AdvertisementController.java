package com.example.demo.web.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.example.demo.web.entity.Advertisement;
import com.example.demo.web.entity.User;
import com.example.demo.web.service.AdvertisementService;
import com.example.demo.web.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Controller
public class AdvertisementController {

    @Autowired
    private AdvertisementService advertisementService;

    /**
     * 获取广告列表
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping(value = "getAdList" , method = RequestMethod.POST)
    @ResponseBody
    public R getAdList(
            @RequestParam("page") Integer page,
            @RequestParam("rows") Integer rows
    ){

        Page<Map<String, Object>> result = advertisementService.selectMapsPage(new Page<Advertisement>(page,rows), new EntityWrapper<Advertisement>());
        return R.ok().put("result",result);
    }

    /**
     * 通过id获取广告信息
     * @param id
     * @return
     */
    @RequestMapping(value = "getAdById" , method = RequestMethod.POST)
    @ResponseBody
    public R getAdById(
            @RequestParam(value = "id") Integer id
    ){
        Advertisement ad = advertisementService.selectById(id);
        return R.ok().put("ad",ad);
    }

    /**
     * 添加广告
     * @param ad
     * @param request
     * @return
     */
    @RequestMapping(value = "addAd" , method = RequestMethod.POST)
    @ResponseBody
    public R addAd(
            @RequestBody Advertisement ad,
            HttpServletRequest request
    ){
        if(ad.getId()==null){
            HttpSession session = request.getSession();
            User u = (User) session.getAttribute("user");
            ad.setAdCreateTime(new Date());
            ad.setAdCreateUser(u.getId());
            advertisementService.insert(ad);
        }else{
            advertisementService.updateById(ad);
        }
        return R.ok();
    }

    /**
     * 通过ID删除
     * @param id
     * @return
     */
    @RequestMapping(value = "delAdById" , method = RequestMethod.POST)
    @ResponseBody
    public R delAdById(
            @RequestParam(value = "id") Integer id
    ){
        advertisementService.deleteById(id);
        return R.ok();
    }
}

