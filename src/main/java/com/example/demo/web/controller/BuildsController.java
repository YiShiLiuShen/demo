package com.example.demo.web.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.example.demo.web.entity.Builds;
import com.example.demo.web.service.BuildsService;
import com.example.demo.web.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Controller
public class BuildsController {

    @Autowired
    private BuildsService buildsService;

    /**
     * 获取楼盘列表
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping(value = "getBuildsList" , method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public R getBuildsList(
            @RequestParam("page") Integer page,
            @RequestParam("rows") Integer rows
    ){
        Map map = new HashMap<>();
        Page<Builds> pages = buildsService.getBuildsByMap(new Page(page,rows),map);
        return R.ok().put("result",pages);
    }
}

