package com.example.demo.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;

/**
 * Created by mjl on 2018/7/17.
 */
public abstract class AbstractController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected boolean isLogin(HttpSession session){
        boolean isLogin = session.getAttribute("isLogin")==null?false: (boolean) session.getAttribute("isLogin");
        return isLogin;
    }

}
