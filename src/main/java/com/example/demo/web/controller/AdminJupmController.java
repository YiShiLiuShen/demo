package com.example.demo.web.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.example.demo.web.entity.City;
import com.example.demo.web.entity.County;
import com.example.demo.web.entity.Province;
import com.example.demo.web.service.CityService;
import com.example.demo.web.service.CountyService;
import com.example.demo.web.service.ProvinceService;
import com.example.demo.web.util.recursion.RecursionDictionary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mjl on 2018/7/16.
 */
@Controller
public class AdminJupmController extends AbstractController {

//    @Autowired
//    private DictionaryService dictionaryService;
//    @Autowired
//    private QRCodeService qrCodeService;
//    @Autowired
//    private HouseService houseService;
    @Autowired
    private CountyService countyService;
    @Autowired
    private CityService cityService;
    @Autowired
    private ProvinceService provinceService;
//    @Autowired
//    private BuildsService buildsService;

    /**
     * 跳转后台管理页面
     * @return
     */
    @RequestMapping(value = "admin", method = RequestMethod.GET)
    public String jumpAdmin(
            HttpServletRequest request,
            HttpServletResponse response
    ){
        return "admin";
    }

    /**
     * 跳转后台管理页面
     * @return
     */
    @RequestMapping(value = "adminMain", method = RequestMethod.GET)
    public String jumpAdminMain(HttpServletRequest request){
//        if(isLogin(request.getSession())){
            return "admin/adminMain";
//        }
//        return "admin";
    }

    /**
     * 广告列表
     * @return
     */
    @RequestMapping(value = "jumpFlowADList", method = RequestMethod.GET)
    public String jumpFlowADList(){
        return "admin/flowADList";
    }


    /**
     * 楼盘
     * @return
     */
    @RequestMapping(value = "jumpBuilds", method = RequestMethod.GET)
    public String jumpBuilds(Model model){
        // 省
        List<Province> provinceList = provinceService.selectList(new EntityWrapper<>());
        List<City> cityList = cityService.selectList(new EntityWrapper<>());
        List<County> countyList = countyService.selectList(new EntityWrapper<>());
        model.addAttribute("provinceList",provinceList);
        model.addAttribute("cityList",cityList);
        model.addAttribute("countyList",countyList);
        return "admin/builds";
    }

//    /**
//     * 户型
//     * @return
//     */
//    @RequestMapping(value = "jumpHouse", method = RequestMethod.GET)
//    public String jumpHouse(
//            Model model
//            ){
//        // 查询楼盘
//        List<BuildsVo> list = buildsService.getBuildsByMap(new HashMap());
//        List<QRCode> qrCodeList = qrCodeService.getAllQRCode();
//        model.addAttribute("buildsList",list);
//        model.addAttribute("qrCodeList",qrCodeList);
//        return "admin/house";
//    }

//    /**
//     * 户型分组
//     * @param model
//     * @return
//     */
//    @RequestMapping(value = "jumpHousePlan", method = RequestMethod.GET)
//    public String jumpHousePlan(
//            Model model
//    ){
//        // 查询楼盘
//        List<BuildsVo> buildsVoList = buildsService.getBuildsByMap(new HashMap());
//        List<HouseVo> houseList = houseService.getHousesByMap(new HashMap());
//        model.addAttribute("buildsVoList",buildsVoList);
//        model.addAttribute("houseList",houseList);
//        return "admin/housePlan";
//    }


    /**
     * 空间方案跳转
     * @return
     */
    @RequestMapping(value = "jumpspacePlan", method = RequestMethod.GET)
    public String jumpspacePlan(){
        return "admin/spacePlan";
    }

    /**
     * 设置单个手机号可查看空间个数
     * @return
     */
    @RequestMapping(value = "jumpUserMobileTimeSet", method = RequestMethod.GET)
    public String jumpUserMobileTimeSet(){
        return "admin/userMobileTimeSet";
    }

    /**
     * 字典管理跳转
     * @return
     */
    @RequestMapping(value = "jumpDictionary", method = RequestMethod.GET)
    public String jumpDictionary(){
        return "admin/dictionary";
    }

    /**
     * 跳转二维码名片
     * @return
     */
    @RequestMapping(value = "jumpQRCode", method = RequestMethod.GET)
    public String jumpQRCode(){
        return "admin/qrCode";
    }

    /**
     * 跳转预约量房
     * @return
     */
    @RequestMapping(value = "jumpReservationRoom", method = RequestMethod.GET)
    public String jumpReservationRoom(){
        return "admin/reservationRoom";
    }

//    /**
//     * 跳转个性化定制
//     * @return
//     */
//    @RequestMapping(value = "jumpPersonalCustomiztion", method = RequestMethod.GET)
//    public String jumpPersonalCustomiztion(Model model){
//        List<Dictionary> dictionaryList = dictionaryService.getAllDictionary();
//        List<Province> provinceList = provinceService.getProvinceByMap(new HashMap());
//        List<City> cityList = cityService.getCityByMap(new HashMap());
//        List<County> countyList = countyService.getCountysByMap(new HashMap());
//        List<Map<String,Object>> listMap = RecursionDictionary.getChild(dictionaryList);
//        model.addAttribute("dictionaryListMap",listMap);
//        model.addAttribute("provinceList",provinceList);
//        model.addAttribute("cityList",cityList);
//        model.addAttribute("countyList",countyList);
//        return "admin/personalCustomiztion";
//    }

    /**
     * 跳转下定有礼
     * @return
     */
    @RequestMapping(value = "jumpXDYLQRCode", method = RequestMethod.GET)
    public String jumpXDYLQRCode(){
        return "admin/xdylqrCode";
    }

    /**
     * 跳转产品中心
     * @return
     */
    @RequestMapping(value = "jumpProduct", method = RequestMethod.GET)
    public String jumpProduct(){
        return "admin/product";
    }
}
