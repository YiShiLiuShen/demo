package com.example.demo.web.aop;

import com.example.demo.web.util.R;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by mjl on 2018/7/28.
 */
//@Component
//@Aspect
public class SessionTimeOutAspect {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public SessionTimeOutAspect() {
    }

    @Pointcut("execution(* com.example.demo.web.controller.admin..*.*(..))")
    public void controllerPointcut(){
    }

    /**
     * 后台登入跳转接口不拦截
     */
    @Pointcut("execution(* com.example.demo.web.controller.admin.AdminJupmController.jumpAdmin(..))")
    public void jumAdminPointcut(){//登录登出功能不需要Session验证
    }

    /**
     *验证码不拦截
     */
    @Pointcut("execution(* com.example.demo.web.controller.admin.CaptchaController.*(..))")
    public void captchaPointcut(){//登录登出功能不需要Session验证
    }

    /**
     * 登入接口不验证
     */
    @Pointcut("execution(* com.example.demo.web.controller.admin.LoginController.*(..))")
    public void loginPointcut(){//登录登出功能不需要Session验证
    }

    @Pointcut("controllerPointcut()")
    public void sessionTimeOutPointcut(){
    }

    @Around("sessionTimeOutPointcut() && !jumAdminPointcut() && !captchaPointcut() && !loginPointcut()")
    public Object sessionTimeOutAdvice(ProceedingJoinPoint pjp) throws Exception {
        Object result = null;
        String targetName = pjp.getTarget().getClass().getSimpleName();
        String methodName = pjp.getSignature().getName();
        logger.info("----------------执行方法-----------------");
        logger.info("类名："+targetName+" 方法名："+methodName);
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes()).getRequest();
//        HttpServletResponse response =((ServletRequestAttributes)
//                RequestContextHolder.getRequestAttributes()).getResponse();
//        for (Object param : pjp.getArgs()) {
//            if (param instanceof HttpServletResponse) {
//                response = (HttpServletResponse) param;
//            }
//        }
        HttpSession session = request.getSession();
        String returnType = ((MethodSignature)pjp.getSignature()).getReturnType().getSimpleName().toString();
        if(session.getAttribute("user")!=null){
            try {
                result = pjp.proceed();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            return result;
        } else{
            logger.debug("Session已超时，正在跳转回登录页面");
            if(returnType.equals("String")){
                return "redirect:/admin";
            }else if(returnType.equals("R")){
                return R.error("Session已超时，请重新登入！");
            }else{
                throw new Exception("Session已超时");
            }
        }
    }

}
