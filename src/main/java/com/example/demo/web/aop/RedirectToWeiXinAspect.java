package com.example.demo.web.aop;

import com.example.demo.web.config.GetSettingPropertiesConfig;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * 手机端访问时跳转到微信接口调取权限
 * Created by mjl on 2018/7/30.
 */

public class RedirectToWeiXinAspect {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("execution(* com.example.demo.web.controller.mobile.MobileJupmController.jumpTempIndex(..))")
    public void jumToWeiXinPointcut(){
    }

//    // 拦截所有手机端页面
//    @Pointcut("execution(* com.example.demo.web.controller.mobile.*.*(..))")
//    public void jumToWeiXinPointcut(){
//    }


    @Around("jumToWeiXinPointcut()")
    public Object RedirectToWeiXinAdvice(ProceedingJoinPoint pjp){
        Object result = null;
        String targetName = pjp.getTarget().getClass().getSimpleName();
        String methodName = pjp.getSignature().getName();
        logger.info("----------------执行方法-----------------");
        logger.info("类名："+targetName+" 方法名："+methodName);
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes()).getRequest();
        HttpServletResponse response =((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getResponse();
        StringBuffer sbf = new StringBuffer("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
        sbf.append(GetSettingPropertiesConfig.getGzhAppid());
        sbf.append("&redirect_uri=");
        sbf.append(GetSettingPropertiesConfig.getGzhSqUrl());
        sbf.append("?datetime="+new Date().getTime());
        sbf.append("&response_type=code");
        sbf.append("&scope=snsapi_userinfo");
        sbf.append("&state=MJL000");
        sbf.append("#wechat_redirect");

        // 直接转发
        return "redirect:"+sbf.toString();

    }

}
