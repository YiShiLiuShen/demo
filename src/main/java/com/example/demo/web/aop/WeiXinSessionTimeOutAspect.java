package com.example.demo.web.aop;

import com.example.demo.web.config.GetSettingPropertiesConfig;
import com.example.demo.web.util.R;
import com.example.demo.web.util.constants.WeiXinUserConstants;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

/**
 * 检测微信是否是登入状态，如果不是，重定向到公众号授权接口
 * Created by mjl on 2018/7/30.
 */
//@Component
//@Aspect
public class WeiXinSessionTimeOutAspect {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 拦截mobile中的所有方法
     */
    @Pointcut("execution(* com.example.demo.web.controller.mobile..*.*(..))")
    public void mobileControllerPointcut(){
    }

    /**
     * 跳转index
     */
    @Pointcut("execution(* com.example.demo.web.controller.mobile.MobileJupmController.jumpIndex(..))")
    public void jumpIndexPointcut(){
    }

    /**
     * 手机验证码不拦截
     */
    @Pointcut("execution(* com.example.demo.web.controller.mobile.MobileCaptchaController.*(..))")
    public void mobileCaptchaPointcut(){//登录登出功能不需要Session验证
    }

    @Around("mobileControllerPointcut() && !jumpIndexPointcut() && !mobileCaptchaPointcut()")
    public Object mobileControllerAround(ProceedingJoinPoint pjp) throws Exception {
        Object result = null;
        String targetName = pjp.getTarget().getClass().getSimpleName();
        String methodName = pjp.getSignature().getName();
        logger.info("----------------执行方法-----------------");
        logger.info("类名："+targetName+" 方法名："+methodName);
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes()).getRequest();
        HttpServletResponse response =((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getResponse();
        String returnType = ((MethodSignature)pjp.getSignature()).getReturnType().getSimpleName().toString();

        for (Object param : pjp.getArgs()) {
            if (param instanceof HttpServletResponse) {
                response = (HttpServletResponse) param;
            }
        }
        HttpSession session = request.getSession();
        if(session.getAttribute(WeiXinUserConstants.WX_USER_INFO)!=null){
            String urlTemp = request.getRequestURI();
            if(request.getRequestURI().replace("/","").equals(request.getContextPath().replace("/",""))){
                try {
                    // 如果session不为空且访问的是根目录，则直接转发到主页
                    request.getRequestDispatcher("index").forward(request,response);
                } catch (ServletException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                result = pjp.proceed();
            } catch (Throwable e) {
                e.printStackTrace();
            }
            return result;
        } else{
            logger.debug("Session已超时，跳转微信接口授权路径");
            StringBuffer sbf = new StringBuffer("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
            sbf.append(GetSettingPropertiesConfig.getGzhAppid());
            sbf.append("&redirect_uri=");
            sbf.append(GetSettingPropertiesConfig.getGzhSqUrl());
            sbf.append("?datetime="+new Date().getTime());
            sbf.append("&response_type=code");
            sbf.append("&scope=snsapi_userinfo");
            sbf.append("&state=MJL000");
            sbf.append("#wechat_redirect");
            return "redirect:"+sbf.toString();
//            if(returnType.equals("String")){
//                return "redirect:"+sbf.toString();
//            }else if(returnType.equals("R")){
//                return R.error("Session已超时，请重新登入！");
//            }else{
//                throw new Exception("Session已超时");
//            }
        }
    }

}
