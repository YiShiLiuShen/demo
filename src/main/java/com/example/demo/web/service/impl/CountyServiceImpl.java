package com.example.demo.web.service.impl;

import com.example.demo.web.entity.County;
import com.example.demo.web.mapper.CountyMapper;
import com.example.demo.web.service.CountyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地址县/区表(tab_county) 服务实现类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Service
public class CountyServiceImpl extends ServiceImpl<CountyMapper, County> implements CountyService {

}
