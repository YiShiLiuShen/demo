package com.example.demo.web.service;

import com.example.demo.web.entity.County;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 地址县/区表(tab_county) 服务类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
public interface CountyService extends IService<County> {

}
