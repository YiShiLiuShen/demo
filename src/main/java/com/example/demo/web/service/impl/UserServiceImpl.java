package com.example.demo.web.service.impl;

import com.example.demo.web.entity.User;
import com.example.demo.web.mapper.UserMapper;
import com.example.demo.web.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
