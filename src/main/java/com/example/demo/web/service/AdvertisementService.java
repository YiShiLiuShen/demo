package com.example.demo.web.service;

import com.example.demo.web.entity.Advertisement;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
public interface AdvertisementService extends IService<Advertisement> {

}
