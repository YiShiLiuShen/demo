package com.example.demo.web.service.impl;

import com.example.demo.web.entity.Province;
import com.example.demo.web.mapper.ProvinceMapper;
import com.example.demo.web.service.ProvinceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地址省表(tab_province) 服务实现类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Service
public class ProvinceServiceImpl extends ServiceImpl<ProvinceMapper, Province> implements ProvinceService {

}
