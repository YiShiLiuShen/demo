package com.example.demo.web.service;

import com.example.demo.web.entity.City;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 地址城市表(tab_city) 服务类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
public interface CityService extends IService<City> {

}
