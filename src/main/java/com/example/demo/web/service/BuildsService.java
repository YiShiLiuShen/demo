package com.example.demo.web.service;


import com.baomidou.mybatisplus.plugins.Page;
import com.example.demo.web.entity.Builds;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
public interface BuildsService extends IService<Builds> {
    Page<Builds> getBuildsByMap(Page<Builds> page,Map map);
}
