package com.example.demo.web.service;

import com.example.demo.web.entity.Province;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 地址省表(tab_province) 服务类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
public interface ProvinceService extends IService<Province> {

}
