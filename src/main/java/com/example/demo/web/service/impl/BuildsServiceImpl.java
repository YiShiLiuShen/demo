package com.example.demo.web.service.impl;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.example.demo.web.entity.Builds;
import com.example.demo.web.mapper.BuildsMapper;
import com.example.demo.web.service.BuildsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Service
public class BuildsServiceImpl extends ServiceImpl<BuildsMapper, Builds> implements BuildsService {
    @Autowired
    private BuildsMapper buildsMapper;

    @Override
    public Page<Builds> getBuildsByMap(Page<Builds> page,Map map) {
        return page.setRecords(buildsMapper.selectBuildsByMap(page,map));
    }
}
