package com.example.demo.web.service.impl;

import com.example.demo.web.entity.Advertisement;
import com.example.demo.web.mapper.AdvertisementMapper;
import com.example.demo.web.service.AdvertisementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Service
public class AdvertisementServiceImpl extends ServiceImpl<AdvertisementMapper, Advertisement> implements AdvertisementService {

}
