package com.example.demo.web.service.impl;

import com.example.demo.web.entity.City;
import com.example.demo.web.mapper.CityMapper;
import com.example.demo.web.service.CityService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地址城市表(tab_city) 服务实现类
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements CityService {

}
