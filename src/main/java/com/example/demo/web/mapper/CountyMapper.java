package com.example.demo.web.mapper;

import com.example.demo.web.entity.County;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 地址县/区表(tab_county) Mapper 接口
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Mapper
public interface CountyMapper extends BaseMapper<County> {

}
