package com.example.demo.web.mapper;

import com.example.demo.web.entity.City;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 地址城市表(tab_city) Mapper 接口
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Mapper
public interface CityMapper extends BaseMapper<City> {

}
