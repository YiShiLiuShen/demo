package com.example.demo.web.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.example.demo.web.entity.Builds;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Mapper
public interface BuildsMapper extends BaseMapper<Builds> {
    List<Builds> selectBuildsByMap(Pagination page, Map map);
}
