package com.example.demo.web.mapper;

import com.example.demo.web.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 好东西带你飞
 * @since 2018-08-15
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    User selectByUserName(String userName);
}
