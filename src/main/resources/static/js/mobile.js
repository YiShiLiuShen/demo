/**
 * Created by Administrator on 2018/8/2.
 */

/***
 * Post请求
 * @param url
 * @param data
 * @param success
 * @param fail
 */
var getRequestPost = function (url, data, success, fail) {
    getRequest('POST', url, data, success, fail);
}

/***
 * 网络请求获取数据
 * @param type
 * @param url
 * @param data
 * @param success
 * @param fail
 */
var getRequest = function (type, url, data, success, fail) {

    $.ajax({
        type : type,
        url  : url,
        cache : false,
        contentType: "application/x-www-form-urlencoded",
        dataType:"json",
        data: data,
        success :function(data){
            success(data);
        },
        error : function(data){
            console.log(data);
            fail(data);
        }
    });
};

/***
 * 统一获取图片路径
 * @param url
 * @returns {*}
 */
var getImage = function (url) {
    var fdStart = url.indexOf("http");
    if (fdStart == 0) {
        return url;
    } else {
        return '/getImage?path=' + url;
    }
};

/***
 * 验证是否是手机号码
 * @param code
 * @returns {boolean}
 */
var isPhoneCode = function (code) {
    //手机号正则
    var phoneReg = /(^1[3|4|5|7|8]\d{9}$)|(^09\d{8}$)/;
    //电话
    if (!phoneReg.test(code)) {
        alert('请输入有效的手机号码！');
        return false;
    }
    return true;
};

/***
 * 判断字符串是否为空
 * @param str
 * @returns {boolean}
 */
var isEmpty = function (str) {
    if (str == undefined || str == '' || str.length == 0) {
        return true;
    }
    return false;
};