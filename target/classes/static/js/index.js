var pathName=window.document.location.pathname;
//截取，得到项目名称
var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);

/**
 * 初始化省市县级联下拉框
 * @param $domProvinc
 * @param provinceList
 * @param $domCity
 * @param cityList
 * @param $domCounty
 * @param countyList
 */
var initUnionProvinceCityCounty = function ($domProvince,provinceList,$domCity,cityList,$domCounty,countyList){
    // 省
    for(var i=0;i<provinceList.length;i++){
        var option = new Option();
        option.label = provinceList[i]["provinceName"];
        option.value = provinceList[i]["provinceId"];
        $('#'+$domProvince).append(option);
    }
    //市
    $('#'+$domProvince).on('change',function(){
        var provinceId = $(this).val();
        $('#'+$domCity+' option').not(":first").remove();
        $('#'+$domCounty+' option').not(":first").remove();
        for(var i=0;i<cityList.length;i++){
            if(provinceId == cityList[i]["provinceId"]){
                var option = new Option();
                option.label = cityList[i]["cityName"];
                option.value = cityList[i]["cityId"];
                $('#'+$domCity).append(option);
            }
        }
    });
    // 县
    $('#'+$domCity).on('change',function(){
        var cityId = $(this).val();
        $('#'+$domCounty+' option').not(":first").remove();
        for(var i=0;i<countyList.length;i++){
            if(cityId == countyList[i]["cityId"]){
                var option = new Option();
                option.label = countyList[i]["regionName"];
                option.value = countyList[i]["regionId"];
                $('#'+$domCounty).append(option);
            }
        }
    });
};

/**
 * 编辑页面初始化省市县级联下拉框
 * @param $domProvince
 * @param buildsProvinceVal
 * @param $domCity
 * @param cityList
 * @param buildsCityVal
 * @param $domCounty
 * @param countyList
 * @param buildsCountyVal
 */
var editUnionProvinceCityCounty = function($domProvince,buildsProvinceVal,$domCity,cityList,buildsCityVal,$domCounty,countyList,buildsCountyVal){
    var buildsProvince = buildsProvinceVal;
    if(buildsProvince!=null && buildsProvince!=undefined && buildsProvince!=""){
        $('#'+$domProvince).val(buildsProvince);
        $('#'+$domCity+' option').not(":first").remove();
        $('#'+$domCounty+' option').not(":first").remove();
        //初始化市
        for(var i=0;i<cityList.length;i++){
            if(cityList[i]["provinceId"]==buildsProvince){
                var option = new Option();
                option.value = cityList[i]["cityId"];
                option.label = cityList[i]["cityName"];
                $("#"+$domCity).append(option);
            }

        }
        var buildsCity = buildsCityVal;
        $('#'+$domCity).val(buildsCity);

        if(buildsCity!=null && buildsCity!=undefined && buildsCity!=""){
            for(var i=0;i<countyList.length;i++){
                if(countyList[i]["cityId"]==buildsCity){
                    var option = new Option();
                    option.value = countyList[i]["regionId"];
                    option.label = countyList[i]["regionName"];
                    $("#"+$domCounty).append(option);
                }
            }
            var buildsCounty = buildsCountyVal;
            $("#"+$domCounty).val(buildsCounty);
        }
    }
};

/**
 * 初始化楼盘 户型 级联下拉框
 * @param $domBuilds
 * @param buildsList
 * @param $domHouse
 * @param houseList
 */
var initUnionBuildsHouse = function($domBuilds,buildsList,$domHouse,houseList){
    for(var i=0;i<buildsList.length;i++){
        var option = new Option();
        option.label = buildsList[i]["buildsName"];
        option.value = buildsList[i]["id"];
        $('#'+$domBuilds).append(option);
    }
    $('#'+$domBuilds).on("change",function(){
        $('#'+$domHouse+' option').not(":first").remove();
        var buildsId = $(this).val();
        for(var i=0;i<houseList.length;i++){
            if(buildsId==houseList[i]["buildsId"]){
                var option = new Option();
                option.value = houseList[i]["id"];
                option.label = houseList[i]["houseName"];
                $('#'+$domHouse).append(option);
            }
        }
    });
};

//编辑页面初始化 楼盘 户型 下拉框
var editUnionBuildsHouse = function(buildsVal,$domHouse,houseList,houseVal){
    var buildsId = buildsVal;
    if(buildsId!=null && buildsId!=undefined && buildsId!=""){
        for(var i=0;i<houseList.length;i++){
            if(buildsId==houseList[i]["buildsId"]){
                var option = new Option();
                option.value = houseList[i]["id"];
                option.label = houseList[i]["houseName"];
                $('#'+$domHouse).append(option);
            }
        }
        if(houseVal!=null && houseVal!=undefined && houseVal!=""){
            $('#'+$domHouse).val(houseVal);
        }
    }
}

/**
 * easyui datagrid 显示图片
 * @param value
 * @param row
 * @param index
 * @returns {string}
 */
var formatImage = function(value,row,index){
    return '<img src="'+projectName+'/getImage?path=' + value + '" height="100" />';
}

var initUpload = function($domInputUpload,$domRealUrl,type){
    $("#"+$domInputUpload).fileinput({
        uploadUrl: projectName+"/upload?type="+type, // server upload action
        uploadAsync: true,
        maxFileCount: 1,
        showBrowse: true,
        showUpload: false, //是否显示上传按钮
        showRemove:true,
        browseOnZoneClick: true,
        dropZoneTitle:"点击这里上传图片(960x640)",
        dropZoneClickTitle:"",
        removeLabel:"移除",
        // uploadLabel:"上传",
        msgPlaceholder:"选择 {files}...",

    });

    //上传操作
    $('#'+$domInputUpload).on('filebatchselected', function(event, files) {
        $('#'+$domInputUpload).fileinput('upload');//上传操作

    });

    // 监听回调操作
    $(document).on('fileuploaded', function(event, data, previewId, index) {
        if(data.response["code"]===0){
            $('.file-preview-thumbnails').html('<img src="'+projectName+'/getImage?path='+data.response["newFilePath"]+'" width="80%">');
            $("#"+$domRealUrl).val(data.response["newFilePath"]);
        }else{
            $("#"+$domRealUrl).val("");
        }
    });

    // 清空
    $("#"+$domInputUpload).on("filecleared",function(event, data, msg){
        $("#"+$domRealUrl).val("");
    });
}



