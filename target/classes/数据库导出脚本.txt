1.导出用户全部表结构
mysqldump -uroot -p -d demo1 > C:\Users\mjl\Documents\demo1.sql
2.导出指定表全部数据
mysqldump -uroot -p -t demo1 tab_city tab_county tab_dictionary tab_province tab_user_mobile_time_set > C:\Users\mjl\Documents\demo1_data_tab_all.sql
3.导出用户表admin用户数据
mysqldump -uroot -p -t demo1 tab_user  --where=" user_name='admin' " > C:\Users\mjl\Documents\demo1_data_tab_user.sql
4.导入数据
	a.创建用户
	b.use 用户；
	c.导入数据：
		source C:\Users\mjl\Documents\demo1.sql;
		source  C:\Users\mjl\Documents\demo1_data_tab_all.sql
		source C:\Users\mjl\Documents\demo1_data_tab_user.sql